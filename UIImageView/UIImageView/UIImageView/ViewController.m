//
//  ViewController.m
//  UIImageView
//
//  Created by 包子 on 2022/9/22.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImage * image = [UIImage imageNamed:@"baozi"];
    UIImageView * imageView = [[UIImageView alloc]init];
    imageView.image = image;
    imageView.backgroundColor = [UIColor blackColor];
    imageView.contentMode = UIViewContentModeCenter;//UIViewContentModeScaleToFill  UIViewContentModeScaleAspectFill
    imageView.clipsToBounds = YES;
    [self.view addSubview:imageView];
    imageView.frame = CGRectMake(0, 40, 48, 100);
}


@end
