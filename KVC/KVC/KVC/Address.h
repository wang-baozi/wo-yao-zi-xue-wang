//
//  Address.h
//  KVC
//
//  Created by 包子 on 2022/5/7.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Address : NSObject

@property(nonatomic, strong) NSString * city;
@property(nonatomic, strong) NSString * street;


@end

NS_ASSUME_NONNULL_END
