//
//  Persion.h
//  KVC
//
//  Created by 包子 on 2022/5/7.
//

#import <Foundation/Foundation.h>
#import "Address.h"

NS_ASSUME_NONNULL_BEGIN

@interface Persion : NSObject

@property(nonatomic, strong)NSString * name;
@property(nonatomic, assign)NSInteger age;
@property(nonatomic, strong)Address * bao;


@end

NS_ASSUME_NONNULL_END
