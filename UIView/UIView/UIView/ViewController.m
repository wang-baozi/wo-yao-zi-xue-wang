//
//  ViewController.m
//  UIView
//
//  Created by 包子 on 2022/9/20.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor yellowColor];
    UIView * customView = [[UIView alloc]init];
    customView.backgroundColor = [UIColor redColor];
    [self.view addSubview:customView];
    customView.frame = CGRectMake(0, 40, 100, 50);
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"按钮" forState:UIControlStateNormal];
    [button setTintColor:[UIColor whiteColor]];
    [customView addSubview:button];
    button.frame = CGRectMake(0, 10, customView.frame.size.width, 30);
}


@end
