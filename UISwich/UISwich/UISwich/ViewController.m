//
//  ViewController.m
//  UISwich
//
//  Created by 包子 on 2022/9/22.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UISwitch * mySwitch = [[UISwitch alloc]init];
    mySwitch.on = NO;
    mySwitch.tintColor = [UIColor greenColor];
    mySwitch.onTintColor = [UIColor redColor];
    mySwitch.backgroundColor = [UIColor yellowColor];
    mySwitch.thumbTintColor = [UIColor yellowColor];
    [self.view addSubview:mySwitch];
    mySwitch.frame = CGRectMake(100, 300, 0, 0);
    
    [mySwitch addTarget:self action:@selector(click) forControlEvents:UIControlEventValueChanged];
    
}

-(void)click{
    NSLog(@"触发");
}

@end
