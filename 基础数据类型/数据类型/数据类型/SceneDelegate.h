//
//  SceneDelegate.h
//  数据类型
//
//  Created by 包子 on 2022/4/27.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

