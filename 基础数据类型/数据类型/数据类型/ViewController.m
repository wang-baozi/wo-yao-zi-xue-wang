//
//  ViewController.m
//  数据类型
//
//  Created by 包子 on 2022/4/27.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //int
    //char
    //array
    //nsstring 字符串
    //nsrange 范围
    //NSNumber 对象
    //数组
    //字典
    //BOOL yes no
    
    NSNumber * baozi = [NSNumber numberWithInt:10];
   // NSLog(@"baozi = %@",baozi);
    
    int  a = 3;
    int b = a + baozi.integerValue;
    //NSLog(@"b = %d",b);
    
    NSString * baozi2 = @"12";
    NSInteger c = baozi2.integerValue;
    NSLog(@"c = %d",c);
    
    
}


@end
