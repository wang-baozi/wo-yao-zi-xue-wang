//
//  ViewController.m
//  回调-目标动作回调
//
//  Created by 包子 on 2022/6/14.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
   
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button addTarget:self action:@selector(click) forControlEvents:UIControlEventTouchUpInside];
    button.frame = CGRectMake(0, 100, 100, 100);
    [self.view addSubview:button];
    button.backgroundColor = [UIColor redColor];
    [button setTitle:@"按钮" forState:UIControlStateNormal];
    
}

-(void)click{
    NSLog(@"我被触发了");
}

@end
