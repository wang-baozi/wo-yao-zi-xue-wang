//
//  SceneDelegate.h
//  回调-目标动作回调
//
//  Created by 包子 on 2022/6/14.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

