//
//  ViewController.m
//  UIPageControl
//
//  Created by 包子 on 2022/9/22.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIPageControl * pageControl = [[UIPageControl alloc]init];
    pageControl.numberOfPages = 5;
    pageControl.currentPage = 0;
    pageControl.currentPageIndicatorTintColor =  [UIColor whiteColor];
    pageControl.pageIndicatorTintColor = [UIColor yellowColor];
    [self.view addSubview: pageControl];
    pageControl.frame = CGRectMake(0, 40, self.view.frame.size.width, 150);
    pageControl.backgroundColor = [UIColor blueColor];
}


@end
