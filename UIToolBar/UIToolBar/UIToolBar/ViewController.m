//
//  ViewController.m
//  UIToolBar
//
//  Created by 包子 on 2022/9/24.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIToolbar * toolBar = [[UIToolbar alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 30)];
    [self.view addSubview:toolBar];
    
    toolBar.backgroundColor = [UIColor orangeColor];
    toolBar.barStyle = UIBarStyleDefault;
    
    UIBarButtonItem * cancelItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(cancelAction:)];
    
    UIBarButtonItem * spaceItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:self action:NULL];
    
    UIBarButtonItem * addItem = [[UIBarButtonItem alloc]initWithBarButtonSystemItem:UIBarButtonSystemItemCancel target:self action:@selector(addItem)];
    
    [toolBar setItems:@[cancelItem,spaceItem,addItem] animated:YES];
}


-(void)cancelAction:(UIBarButtonItem *)sender{
    NSLog(@"取消");
}
-(void)addItem{
    NSLog(@"添加");
    
}



@end
