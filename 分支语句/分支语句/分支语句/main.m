//
//  main.m
//  分支语句
//
//  Created by 包子 on 2022/2/15.
//

#import <Foundation/Foundation.h>
#define book 0

int main(int argc, const char * argv[]) {
    @autoreleasepool {
       int book1 = 20;
        int book2 = book + 10;
        int book3 = book2 - book1;
        int book4 = book2 * book;
        int book5 = book / 3;
        int book6 = book % 3;
        
      /*  NSLog(@"book2 = %d",book2);
        NSLog(@"book3 = %d",book3);
        NSLog(@"book4 = %d",book4);
        NSLog(@"book5 = %d",book5);
        NSLog(@"book6 = %d",book6);*/
        int book7 = book1++;
        int book8 = --book1;
        
      /*  NSLog(@"book7 = %d",book7);
        NSLog(@"book8 = %d",book8);*/
       // book == book1;
       // NSLog(@"结果是%d",book <= book1);
        //book && book1;
        //NSLog(@"结果是%d", !(book || book1));
        
        int book10 = book8&&book?2:3;
        NSLog(@"book10 =%d",book10);
        
    }
    return 0;
}
