//
//  SceneDelegate.h
//  协议
//
//  Created by 包子 on 2022/5/4.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

