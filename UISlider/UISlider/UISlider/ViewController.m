//
//  ViewController.m
//  UISlider
//
//  Created by 包子 on 2022/9/24.
//

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic,strong)UISlider * slider;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.slider = [[UISlider alloc]init];
    self.slider.minimumTrackTintColor = [UIColor redColor];
    [self.slider addTarget:self action:@selector(changeProgress:) forControlEvents:UIControlEventValueChanged];
    
    [self.view addSubview:self.slider];
    
    self.slider.frame = CGRectMake(0 , 200, self.view.frame.size.width, 0);
}
-(void)changeProgress:(UIScreen *)sender{
    NSLog(@"进度 = %f",sender.accessibilityValue);
}

@end
