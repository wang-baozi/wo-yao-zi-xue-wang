//
//  ViewController.m
//  UIDatePicker
//
//  Created by 包子 on 2022/9/24.
//

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic,strong)UIDatePicker * datePicker;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.datePicker = [[UIDatePicker alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    [self.view addSubview:self.datePicker];
    [self.datePicker addTarget:self action:@selector(dateChange) forControlEvents:UIControlEventValueChanged];
    
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:86400];
    self.datePicker.minimumDate = [NSDate dateWithTimeIntervalSinceNow:172800];
    [self.datePicker setDatePickerMode:UIDatePickerModeTime];
    
}

-(void)dateChange:(UIDatePicker *)sender{
    NSLog(@"sender.date =  %@",sender.date);
}

@end
