//
//  SceneDelegate.h
//  oc
//
//  Created by 包子 on 2022/2/25.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

