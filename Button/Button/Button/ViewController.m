//
//  ViewController.m
//  Button
//
//  Created by 包子 on 2022/9/20.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setTitle:@"我是按钮" forState:UIControlStateNormal];
    [button setTitle:@"高亮" forState:UIControlStateHighlighted];
    UIColor * color = [UIColor redColor];
    [button setBackgroundColor:color];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [button setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    [button addTarget:self action:@selector(buttonClick:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    button.frame = CGRectMake(50, 40, 100, 50);
}

- (void)buttonClick:(UIButton *)sender{
    NSLog(@"按钮触发");
}

@end
