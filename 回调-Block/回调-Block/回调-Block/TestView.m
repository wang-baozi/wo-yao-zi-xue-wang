//
//  TestView.m
//  回调-Block
//
//  Created by 包子 on 2022/6/15.
//

#import "TestView.h"

@implementation TestView

- (instancetype)init{
    self = [super init];
    if (self) {
        UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.backgroundColor = [UIColor redColor];
        [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
        [button setTitle:@"点我" forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont systemFontOfSize:18];
        button.frame = CGRectMake(0, 0, 300, 50);
        
        [self addSubview:button];
        
    }
    return self;
    
}

-(void)buttonAction:(UIButton *)sender{
    NSLog(@"触发");
    //self.changeColor(@"包子");
    if (self.changeValueBlock) {
        self.changeValueBlock();
    }
}

-(void) baozi:(void(^)(NSString *str))block;{
    self.changeColor= block;
    
}

- (void)baozi2:(TestBlock)baozi1{
    self.changeColor= baozi1;
}
@end
