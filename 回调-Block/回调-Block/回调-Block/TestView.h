//
//  TestView.h
//  回调-Block
//
//  Created by 包子 on 2022/6/15.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

typedef void(^TestBlock)(NSString *str);

@interface TestView : UIView

@property(nonatomic,strong)void(^changeColor)(NSString * test);

@property(nonatomic,strong)TestBlock baozi1;

//-(void) baozi:(void(^)(NSString *str))block;
- (void)baozi2:(TestBlock)baozi1;

@property(nonatomic,strong)void(^changeValueBlock)(void);

@end

NS_ASSUME_NONNULL_END
