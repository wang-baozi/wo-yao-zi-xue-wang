//
//  SceneDelegate.h
//  回调-Block
//
//  Created by 包子 on 2022/6/15.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

