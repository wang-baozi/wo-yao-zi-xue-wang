//
//  FirstViewController.m
//  回调-Block
//
//  Created by 包子 on 2022/6/16.
//

#import "FirstViewController.h"
#import "TestView.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor redColor];
    [button addTarget:self action:@selector(backAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"返回" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:18];
    button.frame = CGRectMake(0, 100, 300, 50);
    
    [self.view addSubview:button];
    
    TestView * view = [[TestView alloc]init];
    view.frame = CGRectMake(self.view.frame.size.width/2.0 - 150, 300, 300, 50);
    [self.view addSubview:view];
    //循环引用
    
    //Block里面要修改基础类型数值，要在数值前面加上__Block
    __block  NSInteger q = 1;
   
    //要用弱引用，才能释放内存
    __weak typeof(self) baozi3 = self;
    view.changeValueBlock = ^{
        baozi3.view.backgroundColor = [UIColor grayColor];
        q = 3;
        NSLog(@"q = %d",q);
    };
   
    
}

-(void)backAction:(UIButton *)sender{
    [self dismissViewControllerAnimated:YES completion:^{
            
    }];
}

-(void)dealloc{
    NSLog(@"释放");
}
@end
