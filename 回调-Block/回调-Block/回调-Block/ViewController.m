//
//  ViewController.m
//  回调-Block
//
//  Created by 包子 on 2022/6/15.
//

#import "ViewController.h"
#import "TestView.h"
#import "FirstViewController.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    TestView *view = [[TestView alloc]init];
    view.frame = CGRectMake(self.view.frame.size.width/2.0 - 150, 300, 300, 50);
    [self.view addSubview:view];
   // view.changeColor = ^{
     //   self.view.backgroundColor = [UIColor yellowColor];
  //  };
    view.changeColor = ^(NSString * _Nonnull test) {
        NSLog(@"test = %@",test);
    };
    
  //  [view baozi:^(NSString * _Nonnull str) {
   //         NSLog(@"str = %@",str);
   //     self.view.backgroundColor = [UIColor yellowColor];
   // }];
    
    [view baozi2:^(NSString * _Nonnull str) {
        NSLog(@"str = %@",str);
        self.view.backgroundColor = [UIColor yellowColor];
    }];
    
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor redColor];
    [button addTarget:self action:@selector(buttonAction:) forControlEvents:UIControlEventTouchUpInside];
    [button setTitle:@"按钮1" forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont systemFontOfSize:18];
    button.frame = CGRectMake(60, 100, 300, 50);
    
    [self.view addSubview:button];
}

-(void)buttonAction:(UIButton *)sender{
    NSLog(@"按钮1事件");
    //self.view.backgroundColor = [UIColor redColor];
    FirstViewController *firstViewControlelr = [[FirstViewController alloc]init];
    [self presentViewController:firstViewControlelr animated:YES completion:^{
        
    }];
    
}

@end
