//
//  ViewController.m
//  KVO
//
//  Created by 包子 on 2022/5/4.
//

#import "ViewController.h"
#import "Persion.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Persion * baozi = [[Persion alloc]init];
    [baozi addObserver:self forKeyPath:@"name" options:NSKeyValueObservingOptionNew context:nil];
    [baozi addObserver:self forKeyPath:@"age" options:NSKeyValueObservingOptionNew context:nil];
    baozi.name = @"王胜兴";
    baozi.age = 30;
    
}
-(void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context{
    
    NSLog(@"进来了");
    id value = change [NSKeyValueChangeNewKey];
    
    if ([keyPath isEqualToString:@"name"]) {
        NSLog(@"value = %@",value);
        
    }else if ([keyPath isEqualToString:@"age"]){
        NSLog(@"value = %@",value);
    }
}

@end
