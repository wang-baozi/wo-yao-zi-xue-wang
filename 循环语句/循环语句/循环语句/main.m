//
//  main.m
//  循环语句
//
//  Created by 包子 on 2022/2/17.
//

#import <Foundation/Foundation.h>

int main(int argc, const char * argv[]) {
    @autoreleasepool {
    /*   int a = 5;
        while (a>1) {
            a--;
            NSLog(@"a = %d",a);
            */
        
        
      /*  int a = 10;
        int b = 2;
        int c = a + b;
        do{
            
            c = c + b + 1;
            NSLog(@"c = %d",c);
            
        }while (c<20);{
            NSLog(@"我跳出循环了");
        
        }
    }*/
        
     /*   int a = 5;
        int b = 2;
        for (a; a<10; a=a+b){
            NSLog(@"a = %d",a);
        }
    }*/
   /*   int a = 10;
      //  do{
       //    a++;
        //   NSLog(@"a = %d",a);
    // } //break只能终结while语句，不能终结do...while语句
        while (a<20){
            a++;
            NSLog(@"a = %d",a);
            if (a>12) {
                break;
            }
            
        }
        
    }
        NSLog(@"我出循环了");
        
        */
        
        int a = 10;
        do{
           // a++;
            if (a==6) {
                a--;
                continue;//continue是跳出整个循环，break是跳出本次循环
                
            }
            NSLog(@"a = %d",a);
            a=a-1;
        }while(a>3);
        
    }
    
    
    return 0;
}
