//
//  ViewController.m
//  UIActivityindicatorView
//
//  Created by 包子 on 2022/9/22.
//

#import "ViewController.h"

@interface ViewController ()
@property(nonatomic,strong)UIActivityIndicatorView * activityIndicatirView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.activityIndicatirView = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    self.activityIndicatirView.color = [UIColor whiteColor];
    self.activityIndicatirView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.activityIndicatirView];
    self.activityIndicatirView.frame = CGRectMake(100, 100, 100, 100);
    self.activityIndicatirView.hidesWhenStopped = NO;
}
- (IBAction)start:(id)sender {
    [self.activityIndicatirView startAnimating];
}
- (IBAction)stop:(id)sender {
    [self.activityIndicatirView stopAnimating];
}


@end
