//
//  SceneDelegate.h
//  UIActivityindicatorView
//
//  Created by 包子 on 2022/9/22.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

