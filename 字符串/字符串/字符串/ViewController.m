//
//  ViewController.m
//  字符串
//
//  Created by 包子 on 2022/4/15.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    NSString * baozi = @"wangshengxing";
    NSString * baozi1 = [baozi substringToIndex:4];
    //NSLog(@"baozi1 = %@",baozi1);
    NSString *baozi2 = [baozi substringFromIndex:baozi.length -4];
   // NSLog(@"baozi2= %@",baozi2);
    NSRange range;
    range.location = 4;
    range.length = 5;
    NSString * baozi3 = [baozi substringWithRange:range];
    //NSLog(@"baozi3 = %@",baozi3);
    //替换字符串
    NSString * baozi4 =[baozi stringByReplacingOccurrencesOfString:@"a" withString:@"i"];
   // NSLog(@"baozi4 = %@",baozi4);
    //替换某一段字符串
    NSRange range1 = NSMakeRange(4, 5);
    NSString * baozi5 = [baozi stringByReplacingCharactersInRange:range1 withString:@"li"];
   //NSLog(@"baozi5 = %@",baozi5);
    //字符串分割
    
    NSString * baozi6 = @"zhanghao:123，mima:321";
    NSArray * fenge = [baozi6 componentsSeparatedByString:@"，"];
    //NSLog(@"fenge = %@",fenge);
    
    //字符串的拼接

    NSString * baozi7 = @"wang";
    NSString * baozi8 = @"sheng";
    NSString * baozi9 = @"xing";
    NSString * baozi10 = [NSString stringWithFormat:@"%@-%@-%@",baozi7,baozi8,baozi9];
   //NSLog(@"baozi10 = %@",baozi10);
    
    NSString *baozi11 = @"15512863363";
    NSString *baozi12 = [NSString stringWithFormat:@"手机号码：%@",baozi11];
    NSLog(@"baozi12 = %@",baozi12);
}


@end
