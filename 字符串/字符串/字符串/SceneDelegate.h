//
//  SceneDelegate.h
//  字符串
//
//  Created by 包子 on 2022/4/15.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

