//
//  ViewController.m
//  委托模式
//
//  Created by 包子 on 2022/6/15.
//

#import "ViewController.h"
#import "Persion.h"
#import "Clock.h"
@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    Persion * xiaoming = [[Persion alloc]init];
    Clock * clock = [[Clock alloc]init];
    xiaoming.delegate = clock;
    [xiaoming.delegate naoling];
}


@end
