//
//  Naoling.h
//  委托模式
//
//  Created by 包子 on 2022/6/15.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@protocol Naoling <NSObject>

- (void)naoling;

@end

NS_ASSUME_NONNULL_END
