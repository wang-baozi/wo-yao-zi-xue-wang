//
//  ViewController.m
//  UITextField
//
//  Created by 包子 on 2022/9/22.
//

#import "ViewController.h"

@interface ViewController ()<UITextFieldDelegate>

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITextField * textField = [[UITextField alloc]init];
    textField.delegate = self;
    textField.backgroundColor = [UIColor redColor];
    textField.textColor = [UIColor yellowColor];
    textField.font = [UIFont systemFontOfSize:18];
    textField.placeholder = @"请输入账户名";
    [self.view addSubview:textField];
    textField.frame = CGRectMake(0, 50, self.view.frame.size.width, 50);
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField;{
    [textField resignFirstResponder];
    return YES;
}
- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;{
    NSLog(@"开始编辑");
    return YES;
}
- (void)textFieldDidEndEditing:(UITextField *)textField{
    NSLog(@"结束编辑 = %@",textField.text);
}
@end
