//
//  MyClass.h
//  创建类
//
//  Created by 包子 on 2022/3/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyClass : NSObject

@property(nonatomic,strong)NSString *name;
@property(nonatomic,assign)NSInteger sex;
@property(nonatomic,assign)NSInteger age;

@end

NS_ASSUME_NONNULL_END
