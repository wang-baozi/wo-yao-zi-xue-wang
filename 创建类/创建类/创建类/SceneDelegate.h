//
//  SceneDelegate.h
//  创建类
//
//  Created by 包子 on 2022/2/28.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

