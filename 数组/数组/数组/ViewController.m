//
//  ViewController.m
//  数组
//
//  Created by 包子 on 2022/4/28.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //不可变数组
    NSArray * baozi = [[NSArray alloc]initWithObjects:@"wang",@"sheng",@"xing", nil];
    //NSLog(@"baozi = %@",baozi);
    
    
    NSArray * baozi1 = [[NSArray alloc]initWithArray:baozi];
    //NSLog(@"baozi1 = %@",baozi1);
    
    NSArray * baozi2 = [NSArray arrayWithArray:baozi1];
    //NSLog(@"baozi2 = %@",baozi2);
    
  //可变数组
    NSMutableArray * baozi3 = [[NSMutableArray alloc]init];
    NSString * a = @"wang";
    NSString * b = @"sheng";
    NSString * c = @"xing";
    [baozi3 addObject:a];
    [baozi3 addObject:b];
    [baozi3 addObject:c];
   // NSLog(@"baozi3 = %@",baozi3);
    
    
    //操作可变数组
    [baozi3 addObject:[NSNumber numberWithInt:10]];
     [baozi3 addObjectsFromArray:baozi];
     //NSLog(@"baozi3 = %@",baozi3);
    //[baozi3 removeObject:@"wang"];
    [baozi3 removeLastObject];
    [baozi3 removeObjectAtIndex:1];
    [baozi3 removeAllObjects];ß
    NSLog(@"baozi3 = %@",baozi3);
}


@end
