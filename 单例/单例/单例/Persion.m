//
//  Persion.m
//  单例
//
//  Created by 包子 on 2022/5/7.
//

#import "Persion.h"

@implementation Persion

static Persion * _persion;

+(instancetype)allocWithZone:(struct _NSZone *)zone{
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (_persion == nil) {
            _persion = [super allocWithZone:zone];
        }
    });
    return _persion;
}

+ (Persion *)shareInstance{
    return [[self alloc]init];
}

-(id)copyWithZone:(NSZone *)zone{
    return _persion;
}

-(id)mutableCopyWithZone:(NSZone *)zone{
    return _persion;
}

@end
