//
//  ViewController.m
//  UIPickerView
//
//  Created by 包子 on 2022/9/24.
//

#import "ViewController.h"

@interface ViewController ()<UIPickerViewDataSource,UIPickerViewDelegate>
@property(nonatomic,strong)UIPickerView *pickerView;
@property(nonatomic,strong)NSMutableArray * numberArray;
@property(nonatomic,strong)NSMutableArray * titleArray;
@property(nonatomic,strong)NSString * selctedNumStr;
@property(nonatomic,strong)NSString * selctedTitleStr;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.pickerView = [[UIPickerView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, 200)];
    [self.view addSubview:self.pickerView];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    self.view.backgroundColor = [UIColor whiteColor];
    self.pickerView.backgroundColor = [UIColor yellowColor];
    
    self.numberArray = [[NSMutableArray alloc]init];
    for (int i = 0; i<10; i++) {
        NSString * tempStr = [NSString stringWithFormat:@"%d",i];
        [self.numberArray addObject:tempStr];
    }
    self.titleArray = [[NSMutableArray alloc]init];
    for (int i = 0; i<5; i++) {
        NSString * tempStr = [NSString stringWithFormat:@"title%d",i];
        [self.titleArray addObject:tempStr];
    }
}


- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 2;
}

// returns the # of rows in each component..
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    if (component == 0) {
        return self.numberArray.count;
    }else{
        return self.titleArray.count;
    }
}
- (nullable NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component{
        if (component == 0 ) {
            return  self.numberArray[row];
        }else{
            return  self.titleArray[row];
        }
    }
    
- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    
        return 30;
    }

- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component API_UNAVAILABLE(tvos){
    if (component == 0) {
        return 50;
    }
    return 200;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    if (component == 0) {
        NSString * numStr = self.numberArray[row];
        self.selctedNumStr = numStr;
        NSLog(@"numStr = %@",numStr);
    }else{
        NSString * titleStr = self.titleArray[row];
        self.selctedTitleStr = titleStr;
        NSLog(@"titleStr = %@",titleStr);
    }
}




@end
