//
//  ViewController.m
//  UITextView
//
//  Created by 包子 on 2022/9/22.
//

#import "ViewController.h"

@interface ViewController ()<UITextViewDelegate>
@property(nonatomic,strong)UITableView * textView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.textView = [[UITextView alloc]init];
    self.textView.delegate = self;
  //  self.textView.textColor = [UIColor blackColor];
    self.textView.layer.borderWidth = 0.5;
    self.textView.layer.borderColor = [UIColor redColor].CGColor;
    [self.view addSubview:self.textView];
    self.textView.frame = CGRectMake(20, 200, 300, 200);
    
    
}
-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    [self.textView resignFirstResponder];
}
- (BOOL)textViewShouldBeginEditing:(UITextView *)textView{
    NSLog(@"开始编辑");
    return YES;
}
- (void)textViewDidEndEditing:(UITextView *)textView{
    NSLog(@"编辑结束 = %@",textView.text);
}
@end
