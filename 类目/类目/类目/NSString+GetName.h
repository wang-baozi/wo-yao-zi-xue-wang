//
//  NSString+GetName.h
//  类目
//
//  Created by 包子 on 2022/5/3.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface NSString (GetName)

-(NSString * )getName;
-(NSInteger) getAge;
@end

NS_ASSUME_NONNULL_END
