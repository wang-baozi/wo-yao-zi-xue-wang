//
//  SceneDelegate.h
//  类目
//
//  Created by 包子 on 2022/5/3.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

