//
//  FirstViewController.m
//  通知中心
//
//  Created by 包子 on 2022/6/6.
//

#import "FirstViewController.h"
#import "SecondViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"FirestViewController";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor redColor];
    [button setTitle:@"push" forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 100, self.view.frame.size.width, 30);
    [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
    
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(action:) name:@"notifice" object:nil];
}


-(void)click:(UIButton *) sender{
    SecondViewController * fVC = [[SecondViewController alloc]init];
    [self.navigationController pushViewController:fVC animated:YES];
}

-(void)action:(NSNotification *)sender{
    self.view.backgroundColor = [UIColor blueColor];
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
