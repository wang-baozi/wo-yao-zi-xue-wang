//
//  SceneDelegate.h
//  通知中心
//
//  Created by 包子 on 2022/6/2.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

