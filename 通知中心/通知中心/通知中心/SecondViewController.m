//
//  SecondViewController.m
//  通知中心
//
//  Created by 包子 on 2022/6/6.
//

#import "SecondViewController.h"

@interface SecondViewController ()

@end

@implementation SecondViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"SecondViewController";
    self.view.backgroundColor = [UIColor whiteColor];
    
    UIButton * button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor redColor];
    [button setTitle:@"通知中心" forState:UIControlStateNormal];
    button.frame = CGRectMake(0, 100, self.view.frame.size.width, 30);
    [button addTarget:self action:@selector(click:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];
}

-(void)click:(UIButton *) sender{
    [[NSNotificationCenter defaultCenter]postNotificationName:@"notifice" object:nil];
    
}
-(void)dealloc{
    [[NSNotificationCenter defaultCenter]removeObserver:self];
}
@end
