//
//  Book.h
//  类得成员属性与变量
//
//  Created by 包子 on 2022/3/1.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Book : NSObject

@property(nonatomic,strong)NSString * name;

@property(nonatomic,assign)float price;

@property(nonatomic,assign)float yeshu;

@end

NS_ASSUME_NONNULL_END
