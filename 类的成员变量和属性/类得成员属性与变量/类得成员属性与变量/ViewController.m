//
//  ViewController.m
//  类得成员属性与变量
//
//  Created by 包子 on 2022/3/1.
//

#import "ViewController.h"
#import "Book.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    Book * book = [[Book alloc]init];
    book.name = @"我的前半生";
    book.price = 90;
    book.yeshu =80;
    
    self.book1 =[[Book alloc]init];
    self.book1.name = @"我的后半生";
    self.book1.price = 50;
    self.book1.yeshu = 20;
    
    NSMutableArray *array = [[NSMutableArray alloc]init];
    [array addObject:self.book1];
    [array addObject:book];
    
    
}


@end
