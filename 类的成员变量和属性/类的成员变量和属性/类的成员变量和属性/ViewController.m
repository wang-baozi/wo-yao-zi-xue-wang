//
//  ViewController.m
//  类的成员变量和属性
//
//  Created by 包子 on 2022/3/1.
//

#import "ViewController.h"
#import "Book.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.book = [[Book alloc]init];
    self.book.name = @"我的前半生";
    self.book.price = 89;
    self.book.yeshu = 20;
    
    
    Book * book2 = [[Book alloc]init];
    book2.name = @"我的后半生";
    book2.price = 9;
    book2.yeshu = 100;
   
    
    NSMutableArray *array =[[NSMutableArray alloc]init];
    [array addObject:self.book];
    [array addObject:book2];
    
    
    
}


@end
