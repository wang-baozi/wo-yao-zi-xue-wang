//
//  ViewController.m
//  字典
//
//  Created by 包子 on 2022/5/2.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    //不可变字典
    NSDictionary * zidian =[[NSDictionary alloc]init];
    NSDictionary * zidian1 =[NSDictionary dictionaryWithObject:@"baozi" forKey:@"name"];
    NSDictionary * zidian3 = @{
        @"name":@"baozi",
        @"age":@"30"
    };
    NSDictionary * zidian4 = [NSDictionary dictionaryWithDictionary:zidian3];
   // NSLog(@"zidian1 = %@",zidian1);
    NSDictionary * zidian5 = [NSDictionary dictionaryWithObjectsAndKeys:@"name",@"baozi",@"age",@"30", nil];
    //NSLog(@"zidian5 = %@",zidian5);
    //可变字典
    NSMutableDictionary * zidian6 =[[NSMutableDictionary alloc]init];
    NSMutableDictionary * zidian7 =[NSMutableDictionary dictionaryWithObjectsAndKeys:@"name",@"baozi",@"age",@"30", nil];
    //NSLog(@"zidian7 = %@",zidian7);
    
    
    //操作可变字典
    //添加
    [zidian6 setObject:@"wangshengxing" forKey:@"name"];
    [zidian6 setObject:@"30" forKey:@"age"];
    [zidian6 setObject:@"176" forKey:@"height"];
    //NSLog(@"zidian6 = %@",zidian6);
    //修改
    [zidian6 setObject:@"baozi" forKey:@"name"];
    //NSLog(@"zidian6 = %@",zidian6);
    //删除
    //[zidian6 removeObjectForKey:@"age"];
    //[zidian6 removeAllObjects];
   // NSLog(@"zidian = %@",zidian);
    //获取所有的key
    NSArray * shuzu =[zidian6 allKeys];
    NSLog(@"shuzu = %@",shuzu);
    
    //遍历
    for (id key in zidian6) {
        NSLog(@"key:%@, value:%@",key, [zidian6 objectForKey:key]);
    }
}


@end
