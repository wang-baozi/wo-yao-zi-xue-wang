//
//  SceneDelegate.h
//  字典
//
//  Created by 包子 on 2022/5/2.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

